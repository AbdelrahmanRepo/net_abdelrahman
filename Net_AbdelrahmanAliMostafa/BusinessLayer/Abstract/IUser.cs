﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Abstract
{
    public interface IUser
    {
        void AddNewUser(UserModel user);
        void UpdateUserData(int Id, UserModel UpdatedUser);
        void DeleteUser(int Id);
        UserModel GetUserByID(int Id);
        void DeleteSelectedUsers(object obj);
        IEnumerable<UserModel> GetAllUsersData();
    }
}
