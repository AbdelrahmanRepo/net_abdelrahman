﻿using AutoMapper;
using BusinessLayer.Abstract;
using BusinessLayer.Models;
using DatabaseLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Concrete
{
    public class CUsers : IUser
    {
        DBaseContext _context;
        public CUsers()
        {
            _context = new DBaseContext();
        }
        readonly IUser objint;


        public CUsers(IUser interfacevar)
        {
            interfacevar = objint;
        }

        public void AddNewUser(UserModel NewUser)
        {
            _context.Users.Add(Mapper.Map<UserModel, UsersDb>(NewUser));
            _context.SaveChanges();
        }

        public void UpdateUserData(int Id, UserModel UpdatedUser)
        {
            var UserInDB = _context.Users.Where(u => u.UserId == Id).SingleOrDefault();
            Mapper.Map<UserModel, UsersDb>(UpdatedUser, UserInDB);
            _context.SaveChanges();
        }


        public void DeleteUser(int Id)
        {
            var UserInBD = _context.Users.Where(x => x.UserId == Id).FirstOrDefault();
            UserInBD.UserId = 0;
            _context.SaveChanges();
        }

        public UserModel GetUserByID(int Id)
        {
            var result = Mapper.Map<UsersDb, UserModel>(_context.Users.Where(u => u.UserId == Id).SingleOrDefault());
            return result;
        }

        public IEnumerable<UserModel> GetAllUsersData()
        {
            return _context.Users.ToList().Select(Mapper.Map<UsersDb, UserModel>);
        }

        public void DeleteSelectedUsers(object obj)
        {
            IList collection = (IList)obj;
            if (collection.Count == 0)
            {
                var some = _context.Users.Where(x => x.UserId == 0).ToList();
                some.ForEach(a =>
                {
                    a.UserId = 0;
                }
                  );
                _context.SaveChanges();
            }
            else
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    var j = collection[i].ToString();

                    this.DeleteUser(int.Parse(j));
                }
            }
        }
    }
}
