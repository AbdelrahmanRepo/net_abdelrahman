﻿using AutoMapper;
using BusinessLayer.Models;
using DatabaseLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<UsersDb, UserModel>();
            Mapper.CreateMap<UserModel, UsersDb>();
        }
    }
}
