﻿using BusinessLayer.Abstract;
using BusinessLayer.Concrete;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Ninject: NinjectModule
    {
        public override void Load()
        {
            Bind<IUser>().To<CUsers>();
        }
    }
}
