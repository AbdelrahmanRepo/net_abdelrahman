﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Runtime.Remoting.Contexts;

namespace DatabaseLayer
{
    public class DBaseContext : DbContext
    {
        public DBaseContext() : base("con")
        {
            Database.SetInitializer<DBaseContext>(null);
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UsersDb>().ToTable("Users");

        }
        public DbSet<UsersDb> Users { get; set; }
    }
}
