﻿using BusinessLayer.Abstract;
using BusinessLayer.Concrete;
using BusinessLayer.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Net_AbdelrahmanAliMostafa.Controllers.API
{
    public class UsersController : ApiController
    {
        public static StandardKernel skobj = new StandardKernel();
        [HttpPost]
        public void InsertUser(UserModel user)
        {
            skobj.Load(Assembly.GetExecutingAssembly());
            IUser intobj = skobj.Get<CUsers>();
            CUsers specmodel = new CUsers(intobj);
            intobj.AddNewUser(user);
        }
        [HttpPut]
        [Route("api/users/update/{id}")]
        public void UpdateUser(int id, UserModel user)
        {
            skobj.Load(Assembly.GetExecutingAssembly());
            IUser intobj = skobj.Get<CUsers>();
            CUsers specmodel = new CUsers(intobj);
            intobj.UpdateUserData(id, user);
        }
        [HttpDelete]
        public void DeleteUser(int id)
        {
            skobj.Load(Assembly.GetExecutingAssembly());
            IUser intobj = skobj.Get<CUsers>();
            CUsers specmodel = new CUsers(intobj);
            intobj.DeleteUser(id);
        }
        [HttpGet]
        public UserModel GetUser(int id)
        {
            skobj.Load(Assembly.GetExecutingAssembly());
            IUser intobj = skobj.Get<CUsers>();
            CUsers specmodel = new CUsers(intobj);
            return intobj.GetUserByID(id);
        }
        [HttpGet]
        [Route("api/users/all")]
        public IEnumerable<UserModel> GetAllUsersData()
        {
            skobj.Load(Assembly.GetExecutingAssembly());
            IUser intobj = skobj.Get<CUsers>();
            CUsers specmodel = new CUsers(intobj);
            return intobj.GetAllUsersData();
        }
        [HttpPut]
        [Route("api/users/delete")]
        public void DeleteSelected(object obj)
        {
            skobj.Load(Assembly.GetExecutingAssembly());
            IUser intobj = skobj.Get<CUsers>();
            CUsers specmodel = new CUsers(intobj);
            intobj.DeleteSelectedUsers(obj);
        }
    }
}
